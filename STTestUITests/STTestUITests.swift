//
//  STTestUITests.swift
//  STTestUITests
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import XCTest

class STTestUITests: XCTestCase {
    var app: XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        app = XCUIApplication()
        app.launchArguments = ["--Reset"]
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testTapProduct() {
        let app = XCUIApplication()
        let isVisible = app.collectionViews["product_collectionview"].waitForExistence(timeout: 1.0)
        if isVisible {
            let firstChild = app.collectionViews.children(matching:.any).element(boundBy: 0)
            if firstChild.exists {
                 firstChild.tap()
            }
            
            let detail = app.otherElements["detail_viewcontroller"]
            let detailShown = detail.waitForExistence(timeout: 5)
            XCTAssert(detailShown)
            
        }
    }
    
    func testTapFavourites() {
        let isVisible = app.collectionViews["product_collectionview"].waitForExistence(timeout: 1.0)
        if isVisible {
            let firstChild = app.collectionViews.children(matching:.any).element(boundBy: 0)
            if firstChild.exists {
                 firstChild.tap()
            }
            
            let detail = app.otherElements["detail_viewcontroller"]
            let detailShown = detail.waitForExistence(timeout: 5)
            XCTAssert(detailShown)
            
            app.buttons["dynamic_button"].tap()
            app.buttons["dynamic_button"].tap()
            
        }
    }
    
    func testScrollingProduct() {
        let isVisible = app.collectionViews["product_collectionview"].waitForExistence(timeout: 1.0)
        if isVisible {
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            XCTAssertTrue(isVisible, "")
            
        }
    }
    
    func testTapFilterAndClose() {
        let filterButton = app.buttons["filter_button"]
        filterButton.tap()
        let filter = app.otherElements["filter_viewcontroller"]
        let filterShown = filter.waitForExistence(timeout: 5)
        XCTAssert(filterShown)
        
        let closeButton = app.buttons["close_button"]
        closeButton.tap()
        let main = app.otherElements["main_viewcontroller"]
        let mainShown = main.waitForExistence(timeout: 5)
        XCTAssert(mainShown)
    }
    
    func testTapFilterAndTapAll() {
        let filterButton = app.buttons["filter_button"]
        filterButton.tap()
        let filter = app.otherElements["filter_viewcontroller"]
        let filterShown = filter.waitForExistence(timeout: 5)
        XCTAssert(filterShown)
        
        let availableButton = app.buttons["all_button"]
        availableButton.tap()
        
        let main = app.otherElements["main_viewcontroller"]
        let mainShown = main.waitForExistence(timeout: 5)
        XCTAssert(mainShown)
    }
    
    func testTapFilterAndTapAvailable() {
        let filterButton = app.buttons["filter_button"]
        filterButton.tap()
        let filter = app.otherElements["filter_viewcontroller"]
        let filterShown = filter.waitForExistence(timeout: 5)
        XCTAssert(filterShown)
        
        let availableButton = app.buttons["available_button"]
        availableButton.tap()
        
        let main = app.otherElements["main_viewcontroller"]
        let mainShown = main.waitForExistence(timeout: 5)
        XCTAssert(mainShown)
    }
    
    func testFavourites() {
        app.tabBars.firstMatch.buttons.element(boundBy: 1).tap()
        let main = app.otherElements["favourites_viewcontroller"]
        let mainShown = main.waitForExistence(timeout: 5)
        XCTAssert(mainShown)


    }
}

extension XCUIElement {
    /**
     Removes any current text in the field before typing in the new value
     - Parameter text: the text to enter into the field
     */
    func clearAndEnterText(text: String) {
        guard let stringValue = self.value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }

        self.tap()

        let deleteString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: stringValue.count)

        self.typeText(deleteString)
        self.typeText(text)
    }
}
