//
//  Product.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation
struct Product : Codable {
    let name : String?
    let is_available : Bool?
    let images : [String]?
    let desc : String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case is_available = "is_available"
        case images = "images"
        case desc = "description"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        is_available = try values.decodeIfPresent(Bool.self, forKey: .is_available)
        images = try values.decodeIfPresent([String].self, forKey: .images)
        desc = try values.decodeIfPresent(String.self, forKey: .desc)
    }
    
    init(name: String?, isAvailble: Bool?, images: String?, description: String?) {
        self.name = name
        self.is_available = isAvailble
        self.images = [images ?? ""]
        self.desc = description
    }

}
