//
//  FavouritesService.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FavouritesService {
  
    static let sharedManager = FavouritesService()
    var favouritedName: [String] = [String]()
    
    private init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "STTest")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
  
    func insertFavourites(name: String, image: String, productDesc: String, isAvailable: Bool) {
        let managedContext = FavouritesService.sharedManager.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Favourites",
                                            in: managedContext)!
        let favourte = NSManagedObject(entity: entity,
                                 insertInto: managedContext)
    
        favourte.setValue(name, forKeyPath: "name")
        favourte.setValue(image, forKeyPath: "image")
        favourte.setValue(productDesc, forKeyPath: "productDesc")
        favourte.setValue(isAvailable, forKeyPath: "isAvailable")
    
        do {
            favouritedName.append(name)
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
  
    func fetchAllFavourites() -> [Favourites]?{
        let managedContext = FavouritesService.sharedManager.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Favourites")
        do {
            if let favourites = try managedContext.fetch(fetchRequest) as? [Favourites] {
                favouritedName = []
                for favourite in favourites {
                    favouritedName.append(favourite.name ?? "")
                }
                return favourites
            } else {
                return nil
            }
        } catch let error as NSError {
            return nil
        }
    }
  
    func delete(name: String) -> [Favourites]? {
        let managedContext = FavouritesService.sharedManager.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Favourites")
        fetchRequest.predicate = NSPredicate(format: "name == %@" , name)
        do {
            let item = try managedContext.fetch(fetchRequest)
            var arrRemoved = [Favourites]()
            for i in item {
                managedContext.delete(i)
                favouritedName.removeAll { $0 == name }
                try managedContext.save()
                arrRemoved.append(i as! Favourites)
            }
            
            return arrRemoved
      
        } catch let error as NSError {
            return nil
        }
    }
}

