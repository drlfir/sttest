//
//  StyleListResponse.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation
struct StyleListResponse : Codable {
    let product : [Product]?

    enum CodingKeys: String, CodingKey {

        case product = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        product = try values.decodeIfPresent([Product].self, forKey: .product)
    }

}
