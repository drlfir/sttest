//
//  SceneDelegate.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var coordinator: MainCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            coordinator = MainCoordinator(window: window ?? UIWindow(windowScene: windowScene))
            coordinator?.start()
        }
    }

}

