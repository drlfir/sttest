//
//  UIButton+ST.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

extension UIButton {
    func initFilterButton() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = self.isSelected ? UIColor.white.cgColor : UIColor.black.cgColor
        self.clipsToBounds = true
        self.setTitleColor(.white, for: .selected)
        self.setTitleColor(.black, for: .normal)
}
    
    func initFavouriteButton() {
        self.setImage(UIImage(named : "ico-tab-favorites-inactive"), for: .normal)
        self.setImage(UIImage(named : "ico-tab-favorites"), for: .selected)
    }
    
    func initDynamicButton() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = self.isSelected ? UIColor.darkGray.cgColor : UIColor.black.cgColor
        self.clipsToBounds = true
        self.setTitleColor(.white, for: .selected)
        self.setTitleColor(.black, for: .normal)
        self.setTitle(" ADD TO WISHLIST ", for: .normal)
        self.setTitle(" REMOVE FROM WISHLIST ", for: .selected)
        self.backgroundColor = self.isSelected ? .darkGray : .white
    }
}
