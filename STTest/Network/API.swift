//
//  API.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation

class API {
    
    private let BASE_URL = "https://8sp0h28y7j.execute-api.us-east-1.amazonaws.com/dev"
    
    func getProduct(config: ProductsQueryConfig, completion: @escaping (_ products: [Product]?, _ error: String?) -> Void) {
        let baseURL = BASE_URL+"/styles?page_number=\(config.pageNumber)&per_page=\(config.perPage) &is_available=\(config.isAvailable)"
        
        if let encoded = baseURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            guard let url = URL(string: encoded) else {
                print("Url conversion issue.")
                return
            }

            var request = URLRequest(url: url)
            request.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if error == nil && data != nil {
                    do {
                        let jsonDecoder = JSONDecoder()
                        let responseModel = try jsonDecoder.decode(StyleListResponse.self, from: data!)
                        completion(responseModel.product, nil)
                        
                    } catch {
                        completion(nil, "JSONSerialization error")
                    }
                } else if error != nil {
                    completion(nil, "Server error")
                }
            }
            task.resume()
        }

        
    }
    
    init() {
        
    }
}

