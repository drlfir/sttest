//
//  ProductQueryConfig.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation

struct ProductsQueryConfig {
    var perPage: Int
    var pageNumber: Int
    var isAvailable: Bool
    init(perPage: Int = 20,
         pageNumber: Int = 1,
         isAvailable: Bool = false) {
        
        self.perPage = perPage
        self.pageNumber = pageNumber
        self.isAvailable = isAvailable
    }
}

