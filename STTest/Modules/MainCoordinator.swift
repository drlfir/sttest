//
//  AppCoordinator.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject {
    var window: UIWindow
    var childCoordinators = [Coordinator]()
    var styleListNavigationController: UINavigationController?
    var favouritesNavigationController: UINavigationController?
    
    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        FavouritesService.sharedManager.fetchAllFavourites()
        let mainViewController = UITabBarController()
        let styleListViewController = StyleListViewController()
        styleListViewController.coordinator = self
        let favouritesViewController = FavouritesViewController()
        favouritesViewController.coordinator = self
        styleListNavigationController = UINavigationController(rootViewController: styleListViewController)
        favouritesNavigationController = UINavigationController(rootViewController: favouritesViewController)
        
        
        styleListNavigationController?.tabBarItem = UITabBarItem(title: "List",
                                                          image: UIImage(named: "ico-tab-list-inactive"),
                                                          selectedImage: UIImage(named: "ico-tab-list"))
        favouritesNavigationController?.tabBarItem = UITabBarItem(title: "Favourites",
                                                          image: UIImage(named: "ico-tab-favorites-inactive"),
                                                          selectedImage: UIImage(named: "ico-tab-favorites"))
        styleListNavigationController?.tabBarItem.accessibilityIdentifier = "navigation_list"
        favouritesNavigationController?.tabBarItem.accessibilityIdentifier = "navigation_fav"
        mainViewController.viewControllers = [styleListNavigationController ?? UINavigationController(),
                                             favouritesNavigationController ?? UINavigationController()]
        window.rootViewController = mainViewController
        window.makeKeyAndVisible()
    }
    
    func openDetail(fromNavigationController: UINavigationController?, product: Product) {
        let styleDetailViewController = StyleDetailViewController(product: product)
        styleDetailViewController.coordinator = self
        fromNavigationController?.pushViewController(styleDetailViewController, animated: true)
    }
}
