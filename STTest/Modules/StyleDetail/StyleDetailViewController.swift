//
//  StyleDetailViewController.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class StyleDetailViewController: UIViewController {
    weak var coordinator: MainCoordinator?
    let product : Product?
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var dynamicButton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var productImageCollectionView: ProductImageCollectionView!
    
    @IBAction func dynamicButtonOnTapped(_ sender: Any) {
        dynamicButton.isSelected = !dynamicButton.isSelected
        if let product = product {
            if (dynamicButton.isSelected) {
                FavouritesService.sharedManager.insertFavourites(name: product.name ?? "",
                                                                 image: product.images?[0] ?? "",
                                                                 productDesc: product.desc ?? "",
                                                                 isAvailable: product.is_available ?? true)
            } else {
                FavouritesService.sharedManager.delete(name: product.name ?? "")
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "detail_viewcontroller"
        dynamicButton.accessibilityIdentifier = "dynamic_button"
        setupNavigationBar()
        setupProduct()
        dynamicButton.initDynamicButton()
        pageControl.currentPageIndicatorTintColor = UIColor.darkGray
        pageControl.tintColor = UIColor.lightGray
    }
    
    func setupNavigationBar() {
        let logo = UIImage(named: "styletheory-logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    func setupProduct() {
        self.titleLabel.text = product?.name ?? ""
        self.descLabel.text = product?.desc ?? ""
        self.dynamicButton.isSelected = FavouritesService.sharedManager.favouritedName.contains(product?.name ?? "")
        
        productImageCollectionView.collectionViewdelegate = self
        productImageCollectionView.images = product?.images
        pageControl.numberOfPages = product?.images?.count ?? 0
    }

    init(product: Product) {
        self.product = product
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }

}


extension StyleDetailViewController: ProductImageCollectionViewDelegate {
    func pageUpdated(_ index: Int) {
        pageControl.currentPage = index
    }
    
    func onImageTapped() {
        
    }
    
    
}
