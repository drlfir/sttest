//
//  ProductImageCollectionView.swift
//  HappyFresh
//
//  Created by Darul on 22/01/20.
//  Copyright © 2020 HappyFresh Inc. All rights reserved.
//

import SnapKit

protocol ProductImageCollectionViewDelegate {
    func pageUpdated(_ index: Int)
    func onImageTapped()
}

class ProductImageCollectionView: UICollectionView {
    private static let padding: CGFloat = 16
    private static let itemWidth: CGFloat = (UIScreen.main.bounds.size.width - padding)
    private static let itemHeight: CGFloat = 200
    var collectionViewdelegate: ProductImageCollectionViewDelegate?
    var images: [String]? {
        didSet {
            self.reloadData()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        setupView()
    }

    func setupView() {
        decelerationRate = UIScrollView.DecelerationRate.fast
        let layout = SnappingCollectionViewLayout()
        layout.itemSize = CGSize(width: ProductImageCollectionView.itemWidth, height: ProductImageCollectionView.itemHeight)
        layout.minimumInteritemSpacing = ProductImageCollectionView.padding
        layout.minimumLineSpacing = ProductImageCollectionView.padding
        layout.sectionInset = UIEdgeInsets(
            top: ProductImageCollectionView.padding,
            left: ProductImageCollectionView.padding,
            bottom: ProductImageCollectionView.padding,
            right: ProductImageCollectionView.padding
        )
        layout.scrollDirection = .horizontal
        
        isPagingEnabled = true
        isScrollEnabled = true
        collectionViewLayout = layout
        backgroundColor = .white
        showsHorizontalScrollIndicator = false
        isAccessibilityElement = true
        dataSource = self
        delegate = self
        
        
        self.snp.makeConstraints { make in
            make.height.equalTo(ProductImageCollectionView.itemHeight)
            make.width.equalTo(ProductImageCollectionView.itemWidth)
        }
        register(ProductImageCollectionViewCell.self,
                 forCellWithReuseIdentifier: String(describing: ProductImageCollectionViewCell.self))
    }
}

extension ProductImageCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  String(describing: ProductImageCollectionViewCell.self), for: indexPath) as? ProductImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        if let image = images?[indexPath.row] {
            cell.setup(image: image)
        }

        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let pageDelegate = collectionViewdelegate {
            pageDelegate.pageUpdated(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let pageDelegate = collectionViewdelegate {
            pageDelegate.onImageTapped()
        }
    }
}
