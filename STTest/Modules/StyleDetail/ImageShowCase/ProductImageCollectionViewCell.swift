//
//  ProductImageCollectionViewCell.swift
//  HappyFresh
//
//  Created by Darul on 22/01/20.
//  Copyright © 2020 HappyFresh Inc. All rights reserved.
//

import Foundation
import UIKit

class ProductImageCollectionViewCell: UICollectionViewCell {
    lazy var pictureImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(pictureImageView)
        contentView.backgroundColor = .white
        pictureImageView.snp.makeConstraints { make in
            make.top.height.width.bottom.equalToSuperview()
        }
    }
    
    func setup(image: String) {
        pictureImageView.imageFromServerURL(urlString: image)
        pictureImageView.contentMode = UIView.ContentMode.scaleAspectFit
    }
}

