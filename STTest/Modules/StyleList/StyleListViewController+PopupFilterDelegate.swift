//
//  StyleListViewController+PopupFilterDelegate.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation

extension StyleListViewController: PopupFilterViewControllerDelegate {
    func onFilterAllTapped() {
        let config = ProductsQueryConfig(perPage: 20, pageNumber: 1, isAvailable: false)
        productCollectionView.viewModel.config = config
        productCollectionView.viewModel.products.value = []
        productCollectionView.viewModel.getProducts()

    }
    
    func onAvailableOnlyTapped() {
        let config = ProductsQueryConfig(perPage: 20, pageNumber: 1, isAvailable: true)
        productCollectionView.viewModel.config = config
        productCollectionView.viewModel.products.value = []
        productCollectionView.viewModel.getProducts()
    }
    
    
}
