//
//  StyleListViewController.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class StyleListViewController: UIViewController, UICollectionViewDelegate, ProductCollectionViewDelegate {
    @IBOutlet weak var productCollectionView: ProductCollectionView!
    weak var coordinator: MainCoordinator?
    override func viewDidLoad() {
        super.viewDidLoad()
        productCollectionView.productCollectionViewDelegate = self
        setupNavigationBar()
        
        productCollectionView.accessibilityIdentifier = "product_collectionview"
        view.accessibilityIdentifier = "main_viewcontroller"
    }
    
    func setupNavigationBar() {
        let logo = UIImage(named: "styletheory-logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let filterButton = UIButton(type: .custom)
        filterButton.accessibilityIdentifier = "filter_button"
        filterButton.setImage(UIImage(named: "ico-sort"), for: .normal)
        filterButton.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
        filterButton.frame = .zero
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: filterButton)
    }
    
    @objc func filterButtonTapped() {
        let popupFilterViewController = PopupFilterViewController()
        popupFilterViewController.delegate = self
        popupFilterViewController.modalPresentationStyle = .overCurrentContext
        present(popupFilterViewController, animated: true, completion: nil)
        popupFilterViewController.isAvailableSelected = (productCollectionView.viewModel as? ProductCollectionViewModel)?.config.isAvailable ?? false
    }
    
    func isProductSelected(product: Product) {
        coordinator?.openDetail(fromNavigationController: self.navigationController, product: product)
    }
}

