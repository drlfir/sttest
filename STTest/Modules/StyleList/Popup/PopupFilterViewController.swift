//
//  PopupFilterViewController.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

protocol PopupFilterViewControllerDelegate {
    func onFilterAllTapped()
    func onAvailableOnlyTapped()
    
}
class PopupFilterViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var availableButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var overlayView: UIView!
    var delegate: PopupFilterViewControllerDelegate?
    var isAvailableSelected: Bool = false {
        didSet {
            allButton.isSelected = !isAvailableSelected
            availableButton.isSelected = isAvailableSelected
            allButton.backgroundColor = isAvailableSelected ? .white : .black
            availableButton.backgroundColor = isAvailableSelected ? .black : .white
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        closeButton.accessibilityIdentifier = "close_button"
        allButton.accessibilityIdentifier = "all_button"
        availableButton.accessibilityIdentifier = "available_button"
        view.accessibilityIdentifier = "filter_viewcontroller"
        layerView.layer.cornerRadius = 24
        layerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        layerView.layer.masksToBounds = true
        layerView.layer.isOpaque = false
        allButton.initFilterButton()
        availableButton.initFilterButton()
        self.overlayView.alpha = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.2, delay: 0.35, options: [], animations: {
            self.overlayView.alpha = 0.4
        }, completion: nil)
        
    }

    @IBAction func onFilterAllTapped(_ sender: Any) {
        isAvailableSelected = false
        dismiss(animated: false) { [weak self] in
            self?.delegate?.onFilterAllTapped()
        }
    }
    
    @IBAction func onAvailableOnlyTapped(_ sender: Any) {
        isAvailableSelected = true
        dismiss(animated: false) { [weak self] in
            self?.delegate?.onAvailableOnlyTapped()
        }
        
    }
    
    @IBAction func onCloseTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
}
