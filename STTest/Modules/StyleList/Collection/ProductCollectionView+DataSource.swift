//
//  ProductCollectionView+DataSource.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

extension ProductCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.products.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProductCollectionViewCell.self),
                                                      for: indexPath) as! ProductCollectionViewCell
        cell.setup(product: viewModel.products.value[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        productCollectionViewDelegate?.isProductSelected(product: viewModel.products.value[indexPath.item])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if viewModel.isNoMoreProduct.value {
            return
        }
        let indexPathArray = self.indexPathsForVisibleItems
        for indexPath in indexPathArray {
            if (indexPath.item == viewModel.products.value.count - 3) {
                if !viewModel.isScreenLoading.value {
                    viewModel.getProducts()
                }
                break
            }
        }
    }
}
