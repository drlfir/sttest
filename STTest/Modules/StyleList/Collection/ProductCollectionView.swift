//
//  ProductCollectionView.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit
protocol ProductCollectionViewDelegate {
    func isProductSelected(product: Product)
}
class ProductCollectionView: UICollectionView {
    private static let itemWidth: CGFloat = UIScreen.main.bounds.size.width / 2
    private static let itemHeight: CGFloat = 280
    private static let itemSpacing: CGFloat = 0
    let viewModel = ProductCollectionViewModel()
    var productCollectionViewDelegate: ProductCollectionViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        setup()
    }

    func setup() {
        setupView()
        setupObservables()
        viewModel.getProducts()
    }

    func setupView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: ProductCollectionView.itemWidth, height: ProductCollectionView.itemHeight)
        layout.minimumInteritemSpacing = ProductCollectionView.itemSpacing
        layout.minimumLineSpacing = ProductCollectionView.itemSpacing
        layout.scrollDirection = .vertical
        
        contentInset = UIEdgeInsets(
            top: ProductCollectionView.itemSpacing,
            left: ProductCollectionView.itemSpacing,
            bottom: ProductCollectionView.itemSpacing,
            right: ProductCollectionView.itemSpacing
        )
        collectionViewLayout = layout
        backgroundColor = .systemGroupedBackground
        showsHorizontalScrollIndicator = false
        isAccessibilityElement = true
        dataSource = self
        delegate = self

        self.register(UINib(nibName: String(describing: ProductCollectionViewCell.self), bundle: nil),
                      forCellWithReuseIdentifier: String(describing: ProductCollectionViewCell.self))
    }
    
    func setupObservables() {
        viewModel.isFailedOrError.signal.observeValues { isFailedOrError in
            if isFailedOrError {
                self.showError()
            }
        }
        viewModel.products.signal.observeValues { products in
            DispatchQueue.main.async {
                self.reloadData()
            }
        }
    }
    
    func showError() {
        let alert = UIAlertController(title: "Error", message: "Whoops, something is wrong", preferredStyle: .alert)
        if let viewController = UIApplication.shared.windows.first?.rootViewController {
            viewController.present(alert, animated: true, completion: nil)
        }

    }
}

