//
//  StyleListViewModel.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation
import ReactiveSwift

class ProductCollectionViewModel {
    fileprivate(set) var products: MutableProperty<[Product]> = MutableProperty([])
    fileprivate(set) var isScreenLoading = MutableProperty(false)
    fileprivate(set) var isFailedOrError = MutableProperty(false)
    fileprivate(set) var isNoMoreProduct = MutableProperty(false)
    lazy var config: ProductsQueryConfig = ProductsQueryConfig()
    let api: API = API()
    
    func getProducts() {
        isScreenLoading.value = true
        isNoMoreProduct.value = false
        api.getProduct(config: config) { (products, error) in
            self.isFailedOrError.value = error != nil
            self.isScreenLoading.value = false
            if let products = products {
                self.products.value.append(contentsOf: products)
                if products.count == 0 {
                    self.isNoMoreProduct.value = true
                }
            }
        }
    }
}
