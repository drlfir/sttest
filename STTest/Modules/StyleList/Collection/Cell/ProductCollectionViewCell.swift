//
//  ProductCollectionViewCell.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var favouritesButton: UIButton!
    var product: Product?
    
    @IBAction func onFavouritesTapped(_ sender: Any) {
        favouritesButton.isSelected = !favouritesButton.isSelected
        if let product = product {
            if (favouritesButton.isSelected) {
                FavouritesService.sharedManager.insertFavourites(name: product.name ?? "",
                                                                 image: product.images?[0] ?? "",
                                                                 productDesc: product.desc ?? "",
                                                                 isAvailable: product.is_available ?? true)
            } else {
                FavouritesService.sharedManager.delete(name: product.name ?? "")
            }
            
        }
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        availabilityLabel.backgroundColor = .black
        availabilityLabel.layer.cornerRadius = 5
        availabilityLabel.layer.borderWidth = 1
        availabilityLabel.clipsToBounds = true
        availabilityLabel.textColor = .white
        
        favouritesButton.initFavouriteButton()
        favouritesButton.accessibilityIdentifier = "favourites_button"
    }
    
    func setup(product: Product) {
        self.product = product
        if let imageURL = product.images?[0] {
            productImageView.imageFromServerURL(urlString: imageURL)
        }
        productImageView.contentMode = UIView.ContentMode.scaleAspectFill
        availabilityLabel.text = product.is_available ?? true ? "" : " Not Available "
        availabilityLabel.isHidden = product.is_available ?? true
        
        favouritesButton.isSelected = FavouritesService.sharedManager.favouritedName.contains(product.name ?? "")
        
    }

}
