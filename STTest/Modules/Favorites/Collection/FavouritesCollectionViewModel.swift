//
//  FavouritesCollectionViewModel.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import Foundation
import ReactiveSwift

class FavouritesCollectionViewModel {
    fileprivate(set) var products: MutableProperty<[Product]> = MutableProperty([])
    
    func getProducts() {
        products.value = []
        for favourite in FavouritesService.sharedManager.fetchAllFavourites() ?? [] {
            self.products.value.append(Product(name: favourite.name, isAvailble: favourite.isAvailable, images: favourite.image, description: favourite.productDesc))
        }
    }
}
