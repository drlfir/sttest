//
//  FavouritesCollectionView.swift
//  STTest
//
//  Created by Darul on 07/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class FavouritesCollectionView: UICollectionView {
    private static let itemWidth: CGFloat = UIScreen.main.bounds.size.width / 2
    private static let itemHeight: CGFloat = 280
    private static let itemSpacing: CGFloat = 0
    let viewModel = FavouritesCollectionViewModel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        setup()
    }

    func setup() {
        setupView()
        setupObservables()
        viewModel.getProducts()
    }

    func setupView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: FavouritesCollectionView.itemWidth, height: FavouritesCollectionView.itemHeight)
        layout.minimumInteritemSpacing = FavouritesCollectionView.itemSpacing
        layout.minimumLineSpacing = FavouritesCollectionView.itemSpacing
        layout.scrollDirection = .vertical
        
        contentInset = UIEdgeInsets(
            top: FavouritesCollectionView.itemSpacing,
            left: FavouritesCollectionView.itemSpacing,
            bottom: FavouritesCollectionView.itemSpacing,
            right: FavouritesCollectionView.itemSpacing
        )
        collectionViewLayout = layout
        backgroundColor = .systemGroupedBackground
        showsHorizontalScrollIndicator = false
        isAccessibilityElement = true
        dataSource = self

        self.register(UINib(nibName: String(describing: ProductCollectionViewCell.self), bundle: nil),
                      forCellWithReuseIdentifier: String(describing: ProductCollectionViewCell.self))
    }
    
    func setupObservables() {
        viewModel.products.signal.observeValues { products in
            DispatchQueue.main.async {
                self.reloadData()
            }
        }
    }
    
    func showError() {
        let alert = UIAlertController(title: "Error", message: "Whoops, something is wrong", preferredStyle: .alert)
        if let viewController = UIApplication.shared.windows.first?.rootViewController {
            viewController.present(alert, animated: true, completion: nil)
        }

    }
}

