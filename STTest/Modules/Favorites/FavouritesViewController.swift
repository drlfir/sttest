//
//  FavouritesViewController.swift
//  STTest
//
//  Created by Darul on 06/03/20.
//  Copyright © 2020 supersanss. All rights reserved.
//

import UIKit

class FavouritesViewController: UIViewController, UICollectionViewDelegate {
    @IBOutlet weak var favouritesCollectionView: FavouritesCollectionView!
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favouritesCollectionView.delegate = self
        setupNavigationBar()
        self.view.accessibilityIdentifier = "favourites_viewcontroller"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favouritesCollectionView.viewModel.getProducts()
    }
    
    func setupNavigationBar() {
        let logo = UIImage(named: "styletheory-logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        coordinator?.openDetail(fromNavigationController: self.navigationController,
                                product: favouritesCollectionView.viewModel.products.value[indexPath.item])
    }
}
